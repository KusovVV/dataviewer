package com.gmail.victorkusov.dataviewer.utils.constants;

public class Const {
    public static final String BASE_URL = "http://jsonplaceholder.typicode.com";

    public static final String MAPS_PACKAGE = "com.google.android.apps.maps";
    public static final String LOCATION_FORMAT = "geo:0,0?q=%s,%s";
    public static final String MESSAGE_SHOW_INFO = "show-info";
    public static final int ITEMS_PER_ROW = 3;
    public static final String FORMAT_LOG = "%s: Log added";
}
