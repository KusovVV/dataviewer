package com.gmail.victorkusov.dataviewer.di.module;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.gmail.victorkusov.dataviewer.mvp.model.room.db.AppDatabase;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RoomDatabaseModule {

    @Provides
    @Singleton
    AppDatabase provideDatabase(Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, "database")
                .build();
    }
}
