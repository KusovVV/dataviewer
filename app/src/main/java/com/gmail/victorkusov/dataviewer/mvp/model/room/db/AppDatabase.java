package com.gmail.victorkusov.dataviewer.mvp.model.room.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.gmail.victorkusov.dataviewer.mvp.model.room.dao.UserInfoDao;
import com.gmail.victorkusov.dataviewer.mvp.model.room.entity.UserInfoEntity;

@Database(entities = {UserInfoEntity.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract UserInfoDao userInfoDao();
}

