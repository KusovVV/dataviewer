package com.gmail.victorkusov.dataviewer.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

public interface BaseMvpView extends MvpView {

    @StateStrategyType(SkipStrategy.class)
    void handleError(Throwable error);
}
