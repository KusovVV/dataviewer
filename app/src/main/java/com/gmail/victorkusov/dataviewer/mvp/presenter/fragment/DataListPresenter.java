package com.gmail.victorkusov.dataviewer.mvp.presenter.fragment;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.gmail.victorkusov.dataviewer.DataViewerApplication;
import com.gmail.victorkusov.dataviewer.mvp.view.fragment.DataListView;
import com.gmail.victorkusov.dataviewer.mvp.model.rest.ApiInterface;
import com.gmail.victorkusov.dataviewer.mvp.model.rest.model.Card;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class DataListPresenter extends MvpPresenter<DataListView> {

    @Inject
    ApiInterface mApiInterface;

    private List<Card> mData;

    public DataListPresenter() {
        DataViewerApplication.getComponents().inject(this);
    }

    @SuppressLint("CheckResult")
    public void downloadListData() {
        mApiInterface.getCardList()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(response -> {
                            mData = response;
                            getViewState().setupAdapter();
                        },
                        error -> getViewState().handleError(error));
    }


    public List<Card> getData() {
        return mData;
    }

    public int getUserId(int position) {
        return mData.get(position).getUserId();
    }

    public int geId(int position) {
        return mData.get(position).getId();
    }
}
