package com.gmail.victorkusov.dataviewer.view.interfaces;

public interface OnItemClickListener {

    void onItemClick(int position);
}
