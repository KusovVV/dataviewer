package com.gmail.victorkusov.dataviewer.mvp.model.room.entity;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.gmail.victorkusov.dataviewer.mvp.model.rest.model.UserInfo;


@Entity
public class UserInfoEntity {

    @PrimaryKey
    private int id;
    private String name;
    private String username;
    private String email;
    @Embedded (prefix = "address")
    private Address address;
    private String phone;
    private String website;
    @Embedded(prefix = "company")
    private Company company;

    public UserInfoEntity() {
    }

    public UserInfoEntity(UserInfo data) {
        id = data.getId();
        name = data.getName();
        username = data.getUsername();
        email = data.getEmail();
        address = new Address(data.getAddress());
        phone = data.getPhone();
        website = data.getWebsite();
        company = new Company(data.getCompany());
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public Address getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getWebsite() {
        return website;
    }

    public Company getCompany() {
        return company;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
}
