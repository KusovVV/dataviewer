package com.gmail.victorkusov.dataviewer.mvp.model.rest;

import com.gmail.victorkusov.dataviewer.mvp.model.rest.model.Card;
import com.gmail.victorkusov.dataviewer.mvp.model.rest.model.UserInfo;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface ApiInterface {

    @GET("posts")
    Single<List<Card>> getCardList();

    @GET("users/{id}")
    Single<UserInfo> getCardData(@Path(value = "id") int id);
}
