package com.gmail.victorkusov.dataviewer.mvp.view.fragment;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.gmail.victorkusov.dataviewer.mvp.view.BaseMvpView;

public interface DataListView extends BaseMvpView {

    @StateStrategyType(SkipStrategy.class)
    void setupAdapter();
}
