package com.gmail.victorkusov.dataviewer.mvp.model.room.entity;

public class Geo {

    private String lat;
    private String lng;

    public Geo() {
    }

    public Geo(com.gmail.victorkusov.dataviewer.mvp.model.rest.model.Geo data) {
        lat = data.getLat();
        lng = data.getLng();
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}
