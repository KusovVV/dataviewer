package com.gmail.victorkusov.dataviewer.mvp.view.fragment;

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;
import com.gmail.victorkusov.dataviewer.mvp.view.BaseMvpView;

public interface ContactInfoView extends BaseMvpView {
    void initViews();

    @StateStrategyType(SkipStrategy.class)
    void showDataSavedMessage();
}
