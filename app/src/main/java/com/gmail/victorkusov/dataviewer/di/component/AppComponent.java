package com.gmail.victorkusov.dataviewer.di.component;

import com.gmail.victorkusov.dataviewer.di.module.ContextModule;
import com.gmail.victorkusov.dataviewer.di.module.RestClientModule;
import com.gmail.victorkusov.dataviewer.di.module.RoomDatabaseModule;
import com.gmail.victorkusov.dataviewer.mvp.presenter.fragment.ContactInfoPresenter;
import com.gmail.victorkusov.dataviewer.mvp.presenter.fragment.DataListPresenter;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {RestClientModule.class, ContextModule.class,RoomDatabaseModule.class})
@Singleton
public interface AppComponent {

    void inject(ContactInfoPresenter presenter);

    void inject(DataListPresenter dataListPresenter);
}
