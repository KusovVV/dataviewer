package com.gmail.victorkusov.dataviewer.utils;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;

import com.gmail.victorkusov.dataviewer.utils.constants.Const;

import java.util.Calendar;
import java.util.Locale;

public class Utils {


    public static int getScreenWidth(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (wm != null) {
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            return size.x;
        }
        return 0;
    }

    public static String getFileLoggerString() {
        return String.format(String.valueOf(Calendar.getInstance(Locale.getDefault()).getTimeInMillis()), Const.FORMAT_LOG);
    }
}
