package com.gmail.victorkusov.dataviewer.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gmail.victorkusov.dataviewer.R;
import com.gmail.victorkusov.dataviewer.mvp.model.rest.model.Card;
import com.gmail.victorkusov.dataviewer.utils.Utils;
import com.gmail.victorkusov.dataviewer.utils.constants.Const;
import com.gmail.victorkusov.dataviewer.view.interfaces.AdapterController;
import com.gmail.victorkusov.dataviewer.view.interfaces.OnItemClickListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.Holder> implements AdapterController {

    private List<Card> mData;
    private OnItemClickListener mListener;

    public ItemsAdapter() {
        mData = new ArrayList<>();
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new Holder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_note, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        Card item = mData.get(position);

        holder.mItemId.setText(String.valueOf(item.getUserId()));
        holder.mItemTitle.setText(item.getTitle());

        holder.mContainer.setTag(position);
        holder.mContainer.setOnClickListener(v -> mListener.onItemClick((Integer) v.getTag()));
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    @Override
    public void setData(List<Card> data) {
        mData.clear();
        mData.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public void subscribe(OnItemClickListener listener) {
        mListener = listener;
    }

    @Override
    public void unsubscribe() {
        mListener = null;
    }

    public class Holder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_container)
        LinearLayout mContainer;
        @BindView(R.id.item_id)
        TextView mItemId;
        @BindView(R.id.item_title)
        TextView mItemTitle;

        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            ViewGroup.LayoutParams layoutParams = mContainer.getLayoutParams();
            layoutParams.width = Utils.getScreenWidth(itemView.getContext()) / Const.ITEMS_PER_ROW;
            mContainer.setLayoutParams(layoutParams);
        }
    }
}
