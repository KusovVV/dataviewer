package com.gmail.victorkusov.dataviewer.mvp.presenter.fragment;

import android.annotation.SuppressLint;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.gmail.victorkusov.dataviewer.DataViewerApplication;
import com.gmail.victorkusov.dataviewer.mvp.view.fragment.ContactInfoView;
import com.gmail.victorkusov.dataviewer.mvp.model.rest.ApiInterface;
import com.gmail.victorkusov.dataviewer.mvp.model.rest.model.UserInfo;
import com.gmail.victorkusov.dataviewer.mvp.model.room.dao.UserInfoDao;
import com.gmail.victorkusov.dataviewer.mvp.model.room.db.AppDatabase;
import com.gmail.victorkusov.dataviewer.mvp.model.room.entity.UserInfoEntity;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class ContactInfoPresenter extends MvpPresenter<ContactInfoView> {

    @Inject
    ApiInterface mApiInterface;
    @Inject
    AppDatabase mDatabase;

    private UserInfo mData;

    public ContactInfoPresenter() {
        DataViewerApplication.getComponents().inject(this);
    }

    @SuppressLint("CheckResult")
    public void downloadInfoData(int id) {
        mApiInterface.getCardData(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        response -> {
                            mData = response;
                            getViewState().initViews();
                        },
                        error -> getViewState().handleError(error)
                );
    }

    @SuppressLint("CheckResult")
    public void saveInfo() {
        Observable.just(mDatabase.userInfoDao())
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .flatMap((Function<UserInfoDao, ObservableSource<Long>>) dao -> Observable.just(dao.insert(new UserInfoEntity(mData))))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> getViewState().showDataSavedMessage(),
                        error -> getViewState().handleError(error));
    }

    public String getName() {
        return mData.getName();
    }

    public String getUsername() {
        return mData.getUsername();
    }

    public String getEmail() {
        return mData.getEmail();
    }

    public String getWebsite() {
        return mData.getWebsite();
    }

    public String getPhone() {
        return mData.getPhone();
    }

    public String getCity() {
        return mData.getAddress().getCity();
    }

    public String getLat() {
        return mData.getAddress().getGeo().getLat();
    }

    public String getLng() {
        return mData.getAddress().getGeo().getLng();
    }

    public int getId() {
        return mData.getId();
    }
}
