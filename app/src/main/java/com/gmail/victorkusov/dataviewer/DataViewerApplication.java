package com.gmail.victorkusov.dataviewer;

import android.app.Application;

import com.gmail.victorkusov.dataviewer.di.component.AppComponent;
import com.gmail.victorkusov.dataviewer.di.component.DaggerAppComponent;
import com.gmail.victorkusov.dataviewer.di.module.ContextModule;
import com.gmail.victorkusov.dataviewer.di.module.RestClientModule;
import com.gmail.victorkusov.dataviewer.di.module.RoomDatabaseModule;

public class DataViewerApplication extends Application {

    private static AppComponent sComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        sComponent = DaggerAppComponent.builder()
                .contextModule(new ContextModule(this))
                .restClientModule(new RestClientModule())
                .roomDatabaseModule(new RoomDatabaseModule())
                .build();
    }

    public static AppComponent getComponents(){
        return sComponent;
    }

}
