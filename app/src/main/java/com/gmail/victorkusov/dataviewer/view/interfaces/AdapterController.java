package com.gmail.victorkusov.dataviewer.view.interfaces;

import com.gmail.victorkusov.dataviewer.mvp.model.rest.model.Card;

import java.util.List;

public interface AdapterController {

    void setData(List<Card> data);

    void subscribe(OnItemClickListener listener);

    void unsubscribe();
}
