package com.gmail.victorkusov.dataviewer.mvp.model.room.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.gmail.victorkusov.dataviewer.mvp.model.room.entity.UserInfoEntity;

import java.util.List;

import io.reactivex.Single;

@Dao
public interface UserInfoDao {

    @Query("SELECT * FROM UserInfoEntity")
    Single<List<UserInfoEntity>> getAll();

    @Insert (onConflict = OnConflictStrategy.REPLACE)
    Long insert(UserInfoEntity userInfo);
}
