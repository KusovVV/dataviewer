package com.gmail.victorkusov.dataviewer.mvp.model.rest.model;

import com.google.gson.annotations.SerializedName;

public class Company {

    @SerializedName("name")
    private String name;
    @SerializedName("catchPhrase")
    private String catchPhrase;
    @SerializedName("bs")
    private String bs;

    public String getName() {
        return name;
    }

    public String getCatchPhrase() {
        return catchPhrase;
    }

    public String getBs() {
        return bs;
    }
}
