package com.gmail.victorkusov.dataviewer.view.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.gmail.victorkusov.dataviewer.R;
import com.gmail.victorkusov.dataviewer.mvp.presenter.fragment.ContactInfoPresenter;
import com.gmail.victorkusov.dataviewer.mvp.view.fragment.ContactInfoView;
import com.gmail.victorkusov.dataviewer.utils.constants.Const;
import com.gmail.victorkusov.dataviewer.utils.constants.DataKey;

import java.util.Objects;

import butterknife.BindView;
import butterknife.OnClick;

public class ContactInfoFragment extends BaseFragment implements ContactInfoView {

    @BindView(R.id.title)
    TextView mTitle;
    @BindView(R.id.contact_id)
    TextView mContactId;
    @BindView(R.id.name)
    TextView mName;
    @BindView(R.id.login)
    TextView mLogin;
    @BindView(R.id.email)
    TextView mEmail;
    @BindView(R.id.site)
    TextView mSite;
    @BindView(R.id.phone)
    TextView mPhone;
    @BindView(R.id.city)
    TextView mCity;

    @InjectPresenter
    ContactInfoPresenter mPresenter;

    public static ContactInfoFragment getInstance(int userId) {
        Bundle bundle = new Bundle(1);
        bundle.putInt(DataKey.USER_ID, userId);
        ContactInfoFragment fragment = new ContactInfoFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_contact_info;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        int contactId = Objects.requireNonNull(getArguments()).getInt(DataKey.USER_ID);
        mTitle.setText(String.format(getString(R.string.toolbar_title), contactId));
        mPresenter.downloadInfoData(contactId);
    }

    @Override
    public void initViews() {
        mContactId.setText(String.valueOf(mPresenter.getId()));
        mName.setText(mPresenter.getName());
        mLogin.setText(mPresenter.getUsername());
        mEmail.setText(mPresenter.getEmail());
        mSite.setText(mPresenter.getWebsite());
        mPhone.setText(mPresenter.getPhone());
        mCity.setText(mPresenter.getCity());
    }

    @Override
    public void showDataSavedMessage() {
        Toast.makeText(getContext(), "data saved", Toast.LENGTH_SHORT).show();
    }

    @OnClick({R.id.city, R.id.save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.city: {
                Uri location = Uri.parse(String.format(Const.LOCATION_FORMAT, mPresenter.getLat(), mPresenter.getLng()));
                startActivity(new Intent(Intent.ACTION_VIEW)
                        .setPackage(Const.MAPS_PACKAGE)
                        .setData(location));
                break;
            }
            case R.id.save: {
                mPresenter.saveInfo();
                break;
            }
        }
    }
}
