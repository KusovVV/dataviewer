package com.gmail.victorkusov.dataviewer.view.activity;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.gmail.victorkusov.dataviewer.R;
import com.gmail.victorkusov.dataviewer.utils.constants.Const;
import com.gmail.victorkusov.dataviewer.view.fragment.ContactInfoFragment;
import com.gmail.victorkusov.dataviewer.view.fragment.DataListFragment;
import com.gmail.victorkusov.dataviewer.view.model.EventMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, DataListFragment.getInstance())
                .commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Subscribe
    public void onMessageResieved(EventMessage msg){
        if (msg.getMsg().equals(Const.MESSAGE_SHOW_INFO)) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, ContactInfoFragment.getInstance(msg.getId()))
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}
