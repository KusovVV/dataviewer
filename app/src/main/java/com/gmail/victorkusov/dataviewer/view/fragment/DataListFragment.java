package com.gmail.victorkusov.dataviewer.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;
import com.gmail.victorkusov.dataviewer.R;
import com.gmail.victorkusov.dataviewer.mvp.presenter.fragment.DataListPresenter;
import com.gmail.victorkusov.dataviewer.mvp.view.fragment.DataListView;
import com.gmail.victorkusov.dataviewer.utils.Utils;
import com.gmail.victorkusov.dataviewer.utils.constants.Const;
import com.gmail.victorkusov.dataviewer.view.adapter.ItemsAdapter;
import com.gmail.victorkusov.dataviewer.view.interfaces.AdapterController;
import com.gmail.victorkusov.dataviewer.view.model.EventMessage;

import org.greenrobot.eventbus.EventBus;

import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.OnClick;

public class DataListFragment extends BaseFragment implements DataListView {

    @InjectPresenter
    DataListPresenter mPresenter;

    @BindView(R.id.btn_save)
    TextView mBtnSave;
    @BindView(R.id.image)
    AppCompatImageView mImage;
    @BindView(R.id.list_container)
    RecyclerView mListContainer;

    private AdapterController mAdapterController;

    public static DataListFragment getInstance() {
        return new DataListFragment();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mListContainer.setLayoutManager(new GridLayoutManager(getContext(), 2, LinearLayoutManager.HORIZONTAL, false));
        SnapHelper snapHelperStart = new GravitySnapHelper(Gravity.START);
        snapHelperStart.attachToRecyclerView(mListContainer);
        mListContainer.setAdapter(new ItemsAdapter());
        mAdapterController = (AdapterController) mListContainer.getAdapter();

        mPresenter.downloadListData();
    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_list;
    }

    @Override
    public void onStart() {
        super.onStart();
        mAdapterController.subscribe(this::showDataInfo);
    }

    @Override
    public void onResume() {
        super.onResume();
        mImage.startAnimation(getAnimation());
    }

    @Override
    public void onStop() {
        mAdapterController.unsubscribe();
        super.onStop();
    }

    @Override
    public void setupAdapter() {
        mAdapterController.setData(mPresenter.getData());
    }


    @OnClick(R.id.btn_save)
    public void onViewClicked() {
        try {
            Context context = getContext();
            if (context != null) {
                FileOutputStream output = context.openFileOutput("logs.txt", Context.MODE_PRIVATE);
                output.write(Utils.getFileLoggerString().getBytes());
                output.close();
                Toast.makeText(context, "log saved in logs.txt", Toast.LENGTH_SHORT).show();
            }
        } catch (IOException e) {
            handleError(e);
        }
    }

    private Animation getAnimation() {
        ScaleAnimation showScaleAnimation = new ScaleAnimation(0.2f, 1.2f, 0.2f, 1.2f,
                android.view.animation.Animation.RELATIVE_TO_SELF, 0.5f,
                android.view.animation.Animation.RELATIVE_TO_SELF, 0.5f);
        showScaleAnimation.setDuration(500);

        showScaleAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                mBtnSave.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        return showScaleAnimation;
    }

    private void showDataInfo(int position) {
        int userId = mPresenter.getUserId(position);
        EventMessage msg = new EventMessage(Const.MESSAGE_SHOW_INFO, userId);
        EventBus.getDefault().post(msg);
    }
}
