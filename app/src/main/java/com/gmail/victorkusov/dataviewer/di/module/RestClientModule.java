package com.gmail.victorkusov.dataviewer.di.module;

import com.gmail.victorkusov.dataviewer.mvp.model.rest.ApiInterface;
import com.gmail.victorkusov.dataviewer.utils.constants.Const;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RestClientModule {

    @Provides
    @Singleton
    ApiInterface provideApiInterface(){
        return new Retrofit.Builder()
                .baseUrl(Const.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().setLenient().create()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(ApiInterface.class);
    }
}
